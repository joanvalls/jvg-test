JVG Test
===========

Proyecto de prueba. Construcción de una API Rest con las especificaciones adjuntas al documento "Prueba técnica Backend.pdf".

Se ha escogido el framework symfony3 como base y la ayuda de algunos paquetes externos para realizar la api. La razón es que tiene todo lo necesario para las especificaciones requeridas, es conocido, muy usado, estable, hay bastante documentación y gran comunidad (y la version 3 de symfony es LTS).

En cuanto a paquetes externos a symfony se han usado
- guzzlehttp/guzzle para hacer peticiones a servicios externos.
- firebase/php-jwt para la gestión de los tokens de tipo JWT
- zircote/swagger-php para la creacion la documentacion para swagger
- phpunit/phpunit para hacer tests

# Start docker

```sh
$ cd path/to/project
$ docker-compose up -d
$ cd application
$ composer install
```
Al hacer el composer preguntará por algunos valores. Se pueden dejar todos en default (pulsar enter)

Se deben dar permisos de escrituro a la carpeta application/var. Para esta prueba se puede usar 777
```sh
$ chmod -R 777 var
```

Esto dejará todo instalado e iniciado en el host:

```sh
localhost:8100
```

Puedes comprobar los endpoints mediante swagger en la siguiente dirección

Descarga el documento yaml:
```sh
localhost:8100/swagger.php
```

Inline en el navegador
```sh
localhost:8100/swagger.php?inline=1
```
(Puedes copiar el JSON inline y pegarlo en http://editor.swagger.io/)

También se ha añadido un documento para la herramienta postman (JVGTest.postman_collection.json)

**Free Software, Hell Yeah!**