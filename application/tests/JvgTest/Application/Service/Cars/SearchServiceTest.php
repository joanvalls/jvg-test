<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:51
 */

namespace Tests\JvgTest\Application\Service\Auth;

use JvgTest\Application\Service\Cars\SearchRequest;
use JvgTest\Application\Service\Cars\SearchService;
use JvgTest\Infrastructure\Services\Memory\MemoryCarsService;

use PHPUnit\Framework\TestCase;

class SearchServiceTest extends TestCase
{

    public function testSearch_NOCars(){

        $request = new SearchRequest(49.284710, -123.114464, 1);

        $service = new SearchService(
            new MemoryCarsService());

        $response = $service->search($request);

        $this->assertTrue(empty($response));
    }

    public function testSearch_cars1(){

        $request = new SearchRequest(49.284710, -123.114464, 6);

        $service = new SearchService(
            new MemoryCarsService());

        $response = $service->search($request);

        $this->assertTrue((is_array($response) && count($response) === 1));
    }

    public function testSearch_cars2(){

        $request = new SearchRequest(49.284710, -123.114464, 15);

        $service = new SearchService(
            new MemoryCarsService());

        $response = $service->search($request);

        $this->assertTrue((is_array($response) && count($response) === 2));
    }
}
