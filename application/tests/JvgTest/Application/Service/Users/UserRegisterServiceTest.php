<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:51
 */

namespace Tests\JvgTest\Application\Service\Auth;

use JvgTest\Application\Exceptions\AuthException;
use JvgTest\Application\Service\Users\UserRegisterRequest;
use JvgTest\Application\Service\Users\UserRegisterService;
use JvgTest\Infrastructure\Services\Memory\MemoryUsersService;
use PHPUnit\Framework\TestCase;

/**
 * Class UserRegisterServiceTest
 * @package Tests\JvgTest\Application\Service\Auth
 */
class UserRegisterServiceTest extends TestCase
{

    /**
     * @throws \JvgTest\Application\Exceptions\AuthException
     */
    public function testRegisterOK(){

        $request = new UserRegisterRequest("joan123", 1234, "joan");

        $service = new UserRegisterService(
            new MemoryUsersService());

        $response = $service->register($request);

        $this->assertTrue($response);
    }

    /**
     * @throws \JvgTest\Application\Exceptions\AuthException
     */
    public function testRegisterKO_UserExists(){

        $this->expectException(AuthException::class);

        $request = new UserRegisterRequest("usertest", 1234,'testname');

        $service = new UserRegisterService(
            new MemoryUsersService());

        $service->register($request);
    }
}
