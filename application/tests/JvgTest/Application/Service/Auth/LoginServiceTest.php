<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:51
 */

namespace Tests\JvgTest\Application\Service\Auth;

use JvgTest\Application\Exceptions\AuthException;
use JvgTest\Application\Service\Auth\LoginRequest;
use JvgTest\Application\Service\Auth\LoginService;

use JvgTest\Infrastructure\Services\Memory\MemoryUsersService;
use PHPUnit\Framework\TestCase;

class LoginServiceTest extends TestCase
{
    /** @var string  */
    private $secretKey = 'test';

    /** @var array  */
    private $jwtParams = [];

    private $appdir = __DIR__."/../../../../..";

    /**
     * @throws \JvgTest\Application\Exceptions\AuthException
     */
    public function testLoginOK(){

        $request = new LoginRequest("usertest", 1234);

        $service = new LoginService(
            $this->secretKey,
            $this->jwtParams,
            new MemoryUsersService());

        $auth = $service->login($request);

        $this->assertTrue(!empty($auth['token']));
    }

    /**
     * @throws \JvgTest\Application\Exceptions\AuthException
     */
    public function testLoginKO_UserNotExists(){

        $this->expectException(AuthException::class);

        $request = new LoginRequest("test", 1234);

        $service = new LoginService(
            $this->secretKey,
            $this->jwtParams,
            new MemoryUsersService());

        $service->login($request);
    }


    /**
     * @throws \JvgTest\Application\Exceptions\AuthException
     */
    public function testLoginKO_WrongPassword(){

        $this->expectException(AuthException::class);

        $request = new LoginRequest("usertest", 6666);

        $service = new LoginService(
            $this->secretKey,
            $this->jwtParams,
            new MemoryUsersService());

        $service->login($request);
    }
}
