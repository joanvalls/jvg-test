<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 14:54
 */

namespace Tests\AppBundle\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthControllerTest
 * @package Tests\AppBundle\Controller
 */
class CarsControllerTest extends WebTestCase
{
    const HOST = "localhost:8100/api/";

    static function getToken(){
        $client = new Client();

        $url = self::HOST."login";
        $response = $client->post($url, [
            RequestOptions::JSON => ['username' => 'test', 'password' => '1111'],
            'http_errors' => false
        ]);

        $grossData = $response->getBody()->getContents();
        $data = json_decode($grossData);

        return $data->token;
    }

    public function testLoginOk(){

        $token = self::getToken();

        $client = new Client();

        $url = self::HOST."search";
        $response = $client->post($url, [
            RequestOptions::JSON =>
                [
                    'token' => $token,
                    'latitude' => 49.284710,
                    'longitude' => -123.114464,
                    'distance' => 1
                ],
            'http_errors' => false
        ]);
        $grossData = $response->getBody()->getContents();
        $data = json_decode($grossData);

        $this->assertJson($grossData);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertTrue(is_array($data->cars) and count($data->cars) === 59);

        //Check first object is a Car
        $this->checkCarObject($data->cars[0]);
    }

    public function testLogin_noCars(){

        $token = self::getToken();

        $client = new Client();

        $url = self::HOST."search";
        $response = $client->post($url, [
            RequestOptions::JSON =>
                [
                    'token' => $token,
                    'latitude' => 41.297445,
                    'longitude' => 2.08329409,
                    'distance' => 1
                ],
            'http_errors' => false
        ]);
        $grossData = $response->getBody()->getContents();
        $data = json_decode($grossData);

        $this->assertJson($grossData);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertTrue(is_array($data->cars) and count($data->cars) === 0);
    }

    private function checkCarObject($car){
        $this->assertTrue(is_object($car));
        $fields = ['id','make','model','year'];
        foreach($fields as $f){
            $this->assertTrue(isset($car->$f));
        }
    }
}
