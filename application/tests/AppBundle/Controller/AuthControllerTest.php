<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 14:54
 */

namespace Tests\AppBundle\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthControllerTest
 * @package Tests\AppBundle\Controller
 */
class AuthControllerTest extends WebTestCase
{
    const HOST = "localhost:8100/api/";

    public function testLoginOk(){

        $client = new Client();

        $url = self::HOST."login";
        $response = $client->post($url, [
            RequestOptions::JSON => ['username' => 'test', 'password' => '1111'],
            'http_errors' => false
        ]);
        $this->assertJson($response->getBody()->getContents());
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testLoginKo(){

        $client = new Client();

        $url = self::HOST."login";
        $response = $client->post($url, [
            RequestOptions::JSON => ['username' => 'testfake', 'password' => '1111'],
            'http_errors' => false
        ]);
        $this->assertJson($response->getBody()->getContents());
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }
}
