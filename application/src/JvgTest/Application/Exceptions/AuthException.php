<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:24
 */

namespace JvgTest\Application\Exceptions;

/**
 * Class AuthException
 * @package JvgTest\Application\Exceptions
 */
class AuthException extends \Exception
{
    /**
     * AuthException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct($message='Unauthorized', $code=403)
    {
        parent::__construct($message, $code, null);
    }
}