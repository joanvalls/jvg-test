<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Users;

/**
 * Class UserRegisterRequest
 * @package JvgTest\Application\Service\Users
 */
class UserRegisterRequest
{
    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var string */
    private $name;

    /**
     * UserRegisterRequest constructor.
     * @param string $username
     * @param string $password
     * @param string $name
     */
    public function __construct(string $username, string $password, string $name)
    {
        $this->username = $username;
        $this->password = $password;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}