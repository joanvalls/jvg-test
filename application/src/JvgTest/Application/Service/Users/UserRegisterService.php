<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Users;

use JvgTest\Application\Exceptions\AuthException;
use JvgTest\Domain\UsersInterface;


/**
 * Class LoginService
 * @package JvgTest\Application\Service\Auth
 */
class UserRegisterService
{

    const USERNAME_NOT_AVAILABLE = "USERNAME_NOT_AVAILABLE";
    const USER_REGISTERED = "OK";

    /** @var UsersInterface */
    private $userInterface;

    /**
     * UserRegisterService constructor.
     * @param UsersInterface $userInterface
     */
    public function __construct(UsersInterface $userInterface)
    {
        $this->userInterface = $userInterface;
    }

    /**
     * @param UserRegisterRequest $request
     * @return bool
     * @throws AuthException
     */
    public function register(UserRegisterRequest $request)
    {

        $response = $this->userInterface->registerUser(
            $request->getUsername(),
            $request->getPassword(),
            $request->getName());

        if ($response === self::USERNAME_NOT_AVAILABLE) {
            throw new AuthException("Username [{$request->getUsername()}] is not available");
        }

        return true;
    }
}