<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Cars;

use JvgTest\Domain\CarsInterface;

/**
 * Class BookService
 * @package JvgTest\Application\Service\Cars
 */
class BookService
{
    /** @var CarsInterface */
    private $carsInterface;

    /**
     * BookService constructor.
     * @param CarsInterface $carsInterface
     */
    public function __construct(CarsInterface $carsInterface)
    {
        $this->carsInterface = $carsInterface;
    }

    /**
     * @param BookRequest $request
     * @return string
     * @throws \Exception
     */
    public function book(BookRequest $request): string
    {

        $diffdates = $request->getStartdate()->diff($request->getEnddate());
        if($diffdates->invert === 1){
            throw new \Exception("start date cannot be after the end date");
        }

        $car = $this->carsInterface->getCarById($request->getCarId());

        if(empty($car)){
            throw new \Exception("CarId ({$request->getCarId()}) does not exists");
        }

        //TODO: Check if vehicle is still available

        return $this->carsInterface->saveBooking($car, $request->getStartdate(), $request->getEnddate());
    }
}