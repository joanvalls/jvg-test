<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Cars;

/**
 * Class BookRequest
 * @package JvgTest\Application\Service\Cars
 */
class BookRequest
{
    /** @var int */
    private $carId;

    /** @var \DateTime */
    private $startdate;

    /** @var \DateTime */
    private $enddate;

    /**
     * BookRequest constructor.
     * @param int $carId
     * @param \DateTime $startdate
     * @param \DateTime $enddate
     */
    public function __construct(int $carId, \DateTime $startdate, \DateTime $enddate)
    {
        $this->carId = $carId;
        $this->startdate = $startdate;
        $this->enddate = $enddate;
    }

    /**
     * @return int
     */
    public function getCarId(): int
    {
        return $this->carId;
    }

    /**
     * @return \DateTime
     */
    public function getStartdate(): \DateTime
    {
        return $this->startdate;
    }

    /**
     * @return \DateTime
     */
    public function getEnddate(): \DateTime
    {
        return $this->enddate;
    }
}