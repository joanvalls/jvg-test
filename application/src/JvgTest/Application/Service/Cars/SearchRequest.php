<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Cars;

/**
 * Class SearchRequest
 * @package JvgTest\Application\Service\Cars
 */
class SearchRequest
{
    /** @var float */
    private $latitude;

    /** @var float */
    private $longitude;

    /** @var int|null */
    private $distance;

    /**
     * SearchRequest constructor.
     * @param float $latitude
     * @param float $longitude
     * @param int|null $distance
     */
    public function __construct(float $latitude, float $longitude, ?int $distance)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->distance = $distance;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @return int|null
     */
    public function getDistance(): ?int
    {
        return $this->distance;
    }
}