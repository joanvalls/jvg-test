<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Cars;

use JvgTest\Domain\Car;
use JvgTest\Domain\CarsInterface;


/**
 * Class SearchService
 * @package JvgTest\Application\Service\Cars
 */
class SearchService
{

    const DEFAULT_DISTANCE = 10;//km

    /** @var CarsInterface */
    private $carsInterface;

    /**
     * SearchService constructor.
     * @param CarsInterface $carsInterface
     */
    public function __construct(CarsInterface $carsInterface)
    {
        $this->carsInterface = $carsInterface;
    }

    /**
     * @param SearchRequest $request
     * @return array
     */
    public function search(SearchRequest $request): array
    {

        if(empty($request->getDistance())){
            $distance = self::DEFAULT_DISTANCE;
        }else{
            $distance = $request->getDistance();
        }

        $locationsList = $this->carsInterface->getLocations(
            $request->getLatitude(),
            $request->getLongitude(),
            $distance*1000);

        if(empty($locationsList)){
            return [];
        }

        $carList = $this->carsInterface->getCars();

        if(empty($carList)){
            return [];
        }

        $cars = [];
        $locationsCars = $this->sortCarsByLocation($carList);

        foreach($locationsList as $location){
            if(in_array($location->getId(),array_keys($locationsCars))){
                foreach($locationsCars[$location->getId()] as $carIdFound){
                    $cars[] = $carList[$carIdFound];
                }
            }
        }

        $response = [];
        foreach($cars as $car){
            $response[] = $this->presenterCar($car);
        }

        return $response;
    }

    /**
     * @param Car $car
     * @return \stdClass
     */
    private function presenterCar(Car $car): \stdClass
    {
        $pcar = new \stdClass();
        $pcar->id = $car->getId();
        $pcar->make = $car->getMake();
        $pcar->model = $car->getModel();
        $pcar->year = $car->getYear();

        return $pcar;
    }

    /**
     * @param Car[] $carlist
     * @return array
     */
    private function sortCarsByLocation(array $carlist): array
    {
        $locationsCars = [];
        foreach($carlist as $car){
            foreach($car->getLocations() as $loc){
                if(empty($locationsCars[$loc])){
                    $locationsCars[$loc] = [];
                }
                if(!in_array($car->getId(), $locationsCars[$loc])){
                    $locationsCars[$loc][] = $car->getId();
                }
            }
        }
        return $locationsCars;
    }
}