<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Auth;

use Firebase\JWT\JWT;
use JvgTest\Application\Exceptions\AuthException;
use JvgTest\Domain\UsersInterface;


/**
 * Class LoginService
 * @package JvgTest\Application\Service\Auth
 */
class LoginService
{
    /** @var int */
    private $jwtParams;

    /** @var UsersInterface */
    private $userInterface;

    /**
     * LoginService constructor.
     * @param string $jwtKey
     * @param array $jwtParams
     * @param UsersInterface $userInterface
     */
    public function __construct(string $jwtKey, array $jwtParams, UsersInterface $userInterface)
    {
        $this->jwtParams = $jwtParams;
        $this->jwtParams["key"] = $jwtKey;
        $this->userInterface = $userInterface;
    }

    /**
     * @param LoginRequest $request
     * @return array
     * @throws AuthException
     */
    public function login(LoginRequest $request){

        $user = $this->userInterface->authUser($request->getUsername(), $request->getPassword());

        if(empty($user)){
            throw new AuthException("Bad Credentials");
        }

        $payload = [
            "sessionStartDate" => time(),
            "userId" => $user->getId(),
            "userUsername" => $user->getUsername(),
            "userName" => $user->getName()
        ];

        return ["token" => JWT::encode($payload, $this->jwtParams["key"])];
    }
}