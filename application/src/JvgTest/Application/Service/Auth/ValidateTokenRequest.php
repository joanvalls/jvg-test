<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Auth;

/**
 * Class ValidateTokenRequest
 * @package JvgTest\Application\Service\Auth
 */
class ValidateTokenRequest
{
    /** @var string */
    private $token;

    /**
     * ValidateTokenRequest constructor.
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}