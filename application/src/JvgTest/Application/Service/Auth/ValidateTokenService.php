<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:06
 */

namespace JvgTest\Application\Service\Auth;

use Firebase\JWT\JWT;
use JvgTest\Application\Exceptions\AuthException;
use JvgTest\Domain\UsersInterface;


/**
 * Class ValidateTokenService
 * @package JvgTest\Application\Service\Auth
 */
class ValidateTokenService
{
    /** @var int */
    private $jwtParams;

    /**
     * LoginService constructor.
     * @param string $jwtKey
     * @param array $jwtParams
     * @param UsersInterface $userInterface
     */
    public function __construct(string $jwtKey, array $jwtParams)
    {
        $this->jwtParams = $jwtParams;
        $this->jwtParams["key"] = $jwtKey;
    }

    /**
     * @param LoginRequest $request
     * @return array
     * @throws AuthException
     */
    public function validate(ValidateTokenRequest $request){

        try{
            $payload = JWT::decode($request->getToken(), $this->jwtParams["key"], ["HS256"]);
        }catch (\Exception $exception){
            throw new AuthException();
        }

        $timeNow = time();
        if($timeNow - $payload->sessionStartDate > $this->jwtParams["token_ttl"]){
            throw new AuthException("Session has expired");
        }
    }
}