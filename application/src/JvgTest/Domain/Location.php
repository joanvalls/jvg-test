<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:22
 */

namespace JvgTest\Domain;

/**
 * Class Location
 * @package JvgTest\Domain
 */
class Location
{
    /** @var int */
    private $id;

    /** @var float */
    private $distance;

    /** @var float */
    private $latitude;

    /** @var float */
    private $longitude;

    /**
     * Location constructor.
     * @param int $id
     * @param float $distance
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(int $id, float $distance, float $latitude, float $longitude)
    {
        $this->id = $id;
        $this->distance = $distance;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getDistance(): float
    {
        return $this->distance;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }
}