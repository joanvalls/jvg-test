<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 10:33
 */

namespace JvgTest\Domain;

/**
 * Class ApiResponse
 * @package JvgTest\Domain
 */
class ApiResponse
{
    /** @var int */
    private $httpCode;

    /** @var array */
    private $request;

    /** @var mixed */
    private $body;

    /**
     * ApiResponse constructor.
     * @param array|null $request
     */
    public function __construct(?array $request=null)
    {
        $this->request = $request;
        $this->httpCode = 200;
    }

    /**
     * @return array
     */
    public function getRequest(): array
    {
        return $this->request;
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param int $httpCode
     */
    public function setHttpCode(int $httpCode): void
    {
        $this->httpCode = $httpCode;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }
}