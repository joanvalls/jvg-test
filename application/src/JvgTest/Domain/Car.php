<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:22
 */

namespace JvgTest\Domain;

/**
 * Class Car
 * @package JvgTest\Domain
 */
class Car
{
    /** @var int */
    private $id;

    /** @var string */
    private $make;

    /** @var string */
    private $model;

    /** @var int */
    private $year;

    /** @var int[] */
    private $locations;

    /**
     * Car constructor.
     * @param int $id
     * @param string $make
     * @param string $model
     * @param int $year
     * @param array $locations
     */
    public function __construct(int $id, string $make, string $model, int $year, array $locations)
    {
        $this->id = $id;
        $this->make = $make;
        $this->model = $model;
        $this->year = $year;
        $this->locations = $locations;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMake(): string
    {
        return $this->make;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @return int[]
     */
    public function getLocations(): array
    {
        return $this->locations;
    }
}