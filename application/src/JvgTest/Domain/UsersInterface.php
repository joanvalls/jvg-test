<?php

namespace JvgTest\Domain;

/**
 * Interface RestClientInterface
 * @package JvgTest\Domain
 */
interface UsersInterface
{
    /**
     * @param int $userId
     * @return User|null
     */
    public function getUser(int $userId): ?User;

    /**
     * @param string $username
     * @param string $password
     * @return User|null
     */
    public function authUser(string $username, string $password): ?User;

    /**
     * @param string $username
     * @param string $password
     * @param string $name
     * @return User|null
     */
    public function registerUser(string $username, string $password, string $name): string;
}