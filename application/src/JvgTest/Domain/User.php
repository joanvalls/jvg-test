<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:22
 */

namespace JvgTest\Domain;

/**
 * Class User
 * @package JvgTest\Domain
 */
class User
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $username;

    /**
     * User constructor.
     * @param int $id
     * @param string $name
     * @param string $username
     */
    public function __construct(int $id, string $name, string $username)
    {
        $this->id = $id;
        $this->name = $name;
        $this->username = $username;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }
}