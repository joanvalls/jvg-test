<?php

namespace JvgTest\Domain;

/**
 * Interface CarsInterface
 * @package JvgTest\Domain
 */
interface CarsInterface
{
    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $distance
     * @return Location[]
     */
    public function getLocations(float $latitude, float $longitude, int $distance): array;

    /**
     * @return Car[]
     */
    public function getCars(): array;

    /**
     * @param int $id
     * @return Car|null
     */
    public function getCarById(int $id): ?Car;

    /**
     * @param Car $car
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return null|string
     */
    public function saveBooking(Car $car, \DateTime $startDate, \DateTime $endDate): ?string;
}