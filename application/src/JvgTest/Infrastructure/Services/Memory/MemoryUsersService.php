<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:29
 */

namespace JvgTest\Infrastructure\Services\Memory;

use JvgTest\Application\Service\Users\UserRegisterService;
use JvgTest\Domain\User;
use JvgTest\Domain\UsersInterface;

//***** Only for this test. Here we can manage users with databases or an external microservice.

/**
 * Class UsersService
 * @package JvgTest\Infrastructure\Services
 */
class MemoryUsersService implements UsersInterface
{

    /**
     * @var array
     */
    private $userslist = [
        ['id' => 1, 'username' => 'usertest', 'password' => '1234', 'name' => 'testname'],
        ['id' => 2, 'username' => 'usertest2', 'password' => '4567', 'name' => 'testname2']
    ];

    /**
     * @param int $userId
     * @return User|null
     */
    public function getUser(int $userId): ?User
    {

        $user = $this->getUserById($userId);

        if (empty($user)) {
            return null;
        }

        return new User(
            $user['id'],
            $user['name'],
            $user['username']);
    }

    /**
     * @param string $username
     * @param string $password
     * @return User|null
     */
    public function authUser(string $username, string $password): ?User
    {

        $user = $this->getUserByUsername($username);

        if (empty($user)) {
            return null;
        }

        if (!$this->isValid($user, $password)) {
            return null;
        }

        return new User(
            $user['id'],
            $user['name'],
            $user['username']);
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $name
     * @return null|string
     */
    public function registerUser(string $username, string $password, string $name): string
    {
        if ($this->getUserByUsername($username)) {
            //user exists
            return UserRegisterService::USERNAME_NOT_AVAILABLE;
        }

        do{
            $userId = rand(1,99999);
        }while($this->getUserById($userId));

        $this->userslist[] = [
            'id' => $userId,
            'username' => $username,
            'password' => $password,
            'name' => $name
        ];

        return UserRegisterService::USER_REGISTERED;
    }

    /**
     * @param string $username
     * @return array|null
     */
    private function getUserByUsername(string $username): ?array
    {
        foreach ($this->userslist as $user) {
            if ($user['username'] === $username) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @param int $userId
     * @return array|null
     */
    private function getUserById(int $userId): ?array
    {
        foreach ($this->userslist as $user) {
            if ($user['id'] === $userId) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @param array $user
     * @param string $password
     * @return bool
     */
    private function isValid(array $user, string $password)
    {
        return $user['password'] === $password;
    }
}