<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:29
 */

namespace JvgTest\Infrastructure\Services\Memory;

use JvgTest\Domain\Car;
use JvgTest\Domain\CarsInterface;
use JvgTest\Domain\Location;

/**
 * Class CarsService
 * @package JvgTest\Infrastructure\Services
 */
class MemoryCarsService implements CarsInterface
{

    /**
     * @return Car[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCars(): array
    {
        $cars = [];
        $cars[] = [
            'ID' => '1',
            'Make' => 'Audi',
            'Model' => 'Q3',
            'Year' => '2016',
            'Location' => [
                ['LocationID' => '3']
            ]
        ];

        $cars[] = [
            'ID' => '2',
            'Make' => 'Audi',
            'Model' => 'Q3',
            'Year' => '2016',
            'Location' => [
                ['LocationID' => '150']
            ]
        ];

        $cars[] = [
            'ID' => '4',
            'Make' => 'Volkswagen',
            'Model' => 'Golf',
            'Year' => '2005',
            'Location' => [
                ['LocationID' => '290']
            ]
        ];

        $response = [];
        foreach ($cars as $car) {
            $response[intval($car["ID"])] = $this->toCar($car);
        }

        return $response;
    }

    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $distance
     * @return Location[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLocations(float $latitude, float $longitude, int $distance): array
    {
        $locations = [];

        if ($distance > 5000) {
            if ($distance > 10000) {
                $locations[] = [
                    'LocationID' => 290,
                    'Distance' => 5,
                    'Latitude' => 1,
                    'Longitude' => 2
                ];
            }

            $locations[] = [
                'LocationID' => 3,
                'Distance' => 3,
                'Latitude' => 5,
                'Longitude' => 6
            ];
        }

        $response = [];
        foreach ($locations as $location) {
            $response[] = $this->toLocation($location);
        }

        return $response;
    }

    /**
     * @param array $car
     * @return Car
     */
    private function toCar(array $car): Car
    {
        $locations = [];
        foreach ($car['Location'] as $locs) {
            $locations[] = intval($locs['LocationID']);
        }
        return new Car(
            intval($car["ID"]),
            $car["Make"],
            $car["Model"],
            intval($car["Year"]),
            $locations);
    }

    /**
     * @param array $location
     * @return Location
     */
    private function toLocation(array $location): Location
    {
        return new Location(
            intval($location["LocationID"]),
            floatval($location["Distance"]),
            floatval($location["Latitude"]),
            floatval($location["Longitude"]));
    }

    public function getCarById(int $id): ?Car
    {
        if($id === 2){
            $car = [
                'ID' => '2',
                'Make' => 'Audi',
                'Model' => 'Q3',
                'Year' => '2016',
                'Location' => [
                    ['LocationID' => '150']
                ]
            ];
            return $this->toCar($car);
        }

        return null;
    }

    public function saveBooking(Car $car, \DateTime $startDate, \DateTime $endDate): ?string
    {
        $bookingNumber = "BK".rand(100000,999999);

        return $bookingNumber;
    }
}