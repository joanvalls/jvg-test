<?php
/**
 * Created by PhpStorm.
 * User: joan
 * Date: 06/09/18
 * Time: 12:29
 */

namespace JvgTest\Infrastructure\Services;

use GuzzleHttp\Client;
use JvgTest\Domain\Car;
use JvgTest\Domain\CarsInterface;
use JvgTest\Domain\Location;

/**
 * Class CarsService
 * @package JvgTest\Infrastructure\Services
 */
class CarsService implements CarsInterface
{
    /** @var string */
    private $carsDbHost;

    /** @var Client */
    private $restclient;

    /**
     * CarsService constructor.
     * @param string $carsDbHost
     */
    public function __construct(string $carsDbHost)
    {
        $this->carsDbHost = $carsDbHost;
        $this->restclient = new Client();
    }

    /**
     * @return Car[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCars(): array
    {
        $carListResponse = $this->restclient->request('POST', $this->carsDbHost . "v2/car_list", [
            'form_params' => [],
            'http_errors' => false
        ]);

        $list = @json_decode($carListResponse->getBody()->getContents(), true);
        $cars = !empty($list["Response"]["Cars"]) ? $list["Response"]["Cars"] : null;

        if (empty($cars)) {
            return [];
        }

        $response = [];
        foreach ($cars as $car) {
            $response[intval($car["ID"])] = $this->toCar($car);
        }

        return $response;
    }

    /**
     * @param int $id
     * @return Car|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCarById(int $id): ?Car
    {
        $carListResponse = $this->restclient->request('POST', $this->carsDbHost . "v2/car_list", [
            'form_params' => [
                'car_id' => $id
            ],
            'http_errors' => false
        ]);

        $list = @json_decode($carListResponse->getBody()->getContents(), true);
        $cars = !empty($list["Response"]["Cars"]) ? $list["Response"]["Cars"] : null;

        if (empty($cars)) {
            return null;
        }

        return $this->toCar($cars[$id]);
    }

    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $distance
     * @return Location[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLocations(float $latitude, float $longitude, int $distance): array
    {
        $locListResponse = $this->restclient->request('POST', $this->carsDbHost . "v2/nearby", [
            'form_params' => [
                'lat' => $latitude,
                'long' => $longitude,
                'distance' => $distance
            ],
            'http_errors' => false
        ]);

        $list = @json_decode($locListResponse->getBody()->getContents(), true);

        $locations = !empty($list["Response"]["Locations"]) ? $list["Response"]["Locations"] : null;

        if (empty($locations)) {
            return [];
        }

        $response = [];
        foreach ($locations as $location) {
            $response[] = $this->toLocation($location);
        }

        return $response;
    }

    /**
     * @param Car $car
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return null|string
     */
    public function saveBooking(Car $car, \DateTime $startDate, \DateTime $endDate): ?string{

        //TODO: Write to some platform the booking. Database or external microservice

        $bookingNumber = "BK".rand(100000,999999);

        return $bookingNumber;
    }

    /**
     * @param array $car
     * @return Car
     */
    private function toCar(array $car): Car
    {
        $locations = [];
        foreach ($car['Location'] as $locs) {
            $locations[] = intval($locs['LocationID']);
        }
        return new Car(
            intval($car["ID"]),
            $car["Make"],
            $car["Model"],
            intval($car["Year"]),
            $locations);
    }

    /**
     * @param array $location
     * @return Location
     */
    private function toLocation(array $location): Location
    {
        return new Location(
            intval($location["LocationID"]),
            floatval($location["Distance"]),
            floatval($location["Latitude"]),
            floatval($location["Longitude"]));
    }
}