<?php

namespace AppBundle\Controller;

use JvgTest\Domain\ApiResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbsMainController
 * @package AppBundle\Controller
 */
abstract class AbsMainController extends Controller
{
    /**
     * @OA\Info(
     *     title="JVG Test",
     *     version="1.0",
     *     @OA\Contact(
     *          email="joan@joanvalls.cat",
     *          name="Joan Valls Garcia"
     *   ),)
     */

    /**
     * @OA\Server(
     *      url="http://localhost:8100",
     *      description="Docker local server"
     * )
     */

    /**
     * @param Request $request
     * @return array|null
     */
    protected function getRequestData(Request $request): ?array
    {
        return json_decode($request->getContent(), true);
    }

    /**
     * @param ApiResponse $response
     * @return JsonResponse
     */
    protected function output(ApiResponse $response): JsonResponse
    {
        if($response->getHttpCode() !== Response::HTTP_OK){
            return $this->json([
                'errors' => $response->getBody()
            ], $response->getHttpCode() !== 0 ? $response->getHttpCode() : 500);
        }else{
            return $this->json($response->getBody(), $response->getHttpCode());
        }
    }
}
