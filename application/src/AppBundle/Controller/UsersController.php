<?php

namespace AppBundle\Controller;

use JvgTest\Application\Exceptions\AuthException;
use JvgTest\Application\Service\Auth\LoginRequest;
use JvgTest\Application\Service\Users\UserRegisterRequest;
use JvgTest\Domain\ApiResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AuthController
 * @package AppBundle\Controller
 */
class UsersController extends AbsMainController
{
    /**
     * @OA\Post(
     *     path="/api/register",
     *     summary="Register new user",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="username",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 example={"username": "testusername", "password": "2222", "name": "new-name"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Register successfuly. Token sent",
     *         @OA\JsonContent(
     *              type="string",
     *               example="{'token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjgsInVzZXJVc2VybmFtZSI6ImpvYW4iLCJ1c2VyTmFtZSI6IkpvaGFuXG4ifQ.ibEEy9yOQ40CaktQiohhN8lsXPzPZNUkDgvRvcTNzsA'}"
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Login error",
     *     )
     * )
     *
     *
     */
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function registerAction(Request $request): JsonResponse
    {
        $data = $this->getRequestData($request);
        $response = new ApiResponse($data);

        try {
            $requiredFields = ['username', 'password', 'name'];
            $this->validateRequest($requiredFields, $data);

            $this->get("jvgtest.application.service.users.user_register_service")->register(
                new UserRegisterRequest($data['username'], $data['password'], $data['name']));

            $auth = $this->get("jvgtest.application.service.auth.login_service")->login(
                new LoginRequest($data['username'], $data['password']));

            $response->setBody($auth);
        } catch (\Exception $exception) {
            $response->setHttpCode($exception->getCode());
            $response->setBody($exception->getMessage());
        }

        return $this->output($response);
    }

    /**
     * @param array $requiredFields
     * @param array $requestData
     * @throws \Exception
     */
    private function validateRequest(array $requiredFields, array $requestData): void
    {
        foreach ($requiredFields as $field) {
            if (empty($requestData[$field])) {
                throw new \Exception("{$field} not provided", 400);
            }
        }
    }
}