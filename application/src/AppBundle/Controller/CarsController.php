<?php

namespace AppBundle\Controller;

use JvgTest\Application\Service\Auth\ValidateTokenRequest;
use JvgTest\Application\Service\Cars\BookRequest;
use JvgTest\Application\Service\Cars\SearchRequest;
use JvgTest\Domain\ApiResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AuthController
 * @package AppBundle\Controller
 */
class CarsController extends AbsMainController
{
    /**
     * @OA\Post(
     *     path="/api/search",
     *     summary="Search cars around a geolocation point",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="token",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="latitude",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="longitude",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="distance",
     *                     type="integer"
     *                 ),
     *                 example={
     *                          "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uU3RhcnREYXRlIjoxNTM2MjQ5NjUyLCJ1c2VySWQiOjEsInVzZXJVc2VybmFtZSI6InRlc3QiLCJ1c2VyTmFtZSI6IlRlc3ROYW1lXG4ifQ.olcei3tWxN35ZoX5_oDqkW5zKtAoB-ntlAvucvfA5Eg",
     *                          "latitude": 49.284710,
     *                          "longitude": -123.114464,
     *                          "distance": 1
     *                      }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Get car list",
     *         @OA\JsonContent(
     *              type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Invalid token",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Missing parameters",
     *     )
     * )
     *
     *
     */
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchAction(Request $request): JsonResponse
    {
        $data = $this->getRequestData($request);
        $response = new ApiResponse($data);

        try{
            $requiredFields = ['token','latitude', 'longitude'];
            $this->validateRequest($requiredFields, $data);

            $this->get("jvgtest.application.service.auth.validate_token_service")->validate(
                new ValidateTokenRequest($data["token"])
            );

            $list = $this->get("jvgtest.application.service.cars.search_service")->search(
                new SearchRequest($data["latitude"], $data["longitude"], !empty($data["distance"]) ? $data["distance"] : null));


            $response->setBody(["cars" => $list]);
        }catch(\Exception $exception){
            $response->setHttpCode($exception->getCode());
            $response->setBody($exception->getMessage());
        }

        return $this->output($response);
    }

    /**
     * @OA\Post(
     *     path="/api/book",
     *     summary="Book a car",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="token",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="start",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="end",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="carId",
     *                     type="integer"
     *                 ),
     *                 example={
     *                          "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uU3RhcnREYXRlIjoxNTM2MjQ5NjUyLCJ1c2VySWQiOjEsInVzZXJVc2VybmFtZSI6InRlc3QiLCJ1c2VyTmFtZSI6IlRlc3ROYW1lXG4ifQ.olcei3tWxN35ZoX5_oDqkW5zKtAoB-ntlAvucvfA5Eg",
     *                          "start": "2018-09-21 10:00",
     *                          "end": "2018-09-21 22:00",
     *                          "carId": 3
     *                      }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Booking confirmed",
     *         @OA\JsonContent(
     *              type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Invalid token",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Missing parameters",
     *     )
     * )
     *
     *
     */
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function bookAction(Request $request): JsonResponse
    {
        $data = $this->getRequestData($request);
        $response = new ApiResponse($data);

        try{
            $requiredFields = ['token','start', 'end','carId'];
            $this->validateRequest($requiredFields, $data);

            $this->get("jvgtest.application.service.auth.validate_token_service")->validate(
                new ValidateTokenRequest($data["token"])
            );

            $startDate = new \DateTime($data["start"], new \DateTimeZone("GMT"));
            $endDate = new \DateTime($data["end"], new \DateTimeZone("GMT"));

            $bookingNumber = $this->get("jvgtest.application.service.cars.book_service")->book(
                new BookRequest($data["carId"], $startDate, $endDate));

            $response->setBody(["bookingNumber" => $bookingNumber]);
        }catch(\Exception $exception){
            $response->setHttpCode($exception->getCode());
            $response->setBody($exception->getMessage());
        }

        return $this->output($response);
    }

    /**
     * @param array $requiredFields
     * @param array|null $requestData
     * @throws \Exception
     */
    private function validateRequest(array $requiredFields, ?array $requestData): void
    {
        foreach($requiredFields as $field){
            if(empty($requestData[$field])){
                throw new \Exception("{$field} not provided",400);
            }
        }
    }
}
