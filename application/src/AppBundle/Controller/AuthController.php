<?php

namespace AppBundle\Controller;

use JvgTest\Application\Exceptions\AuthException;
use JvgTest\Application\Service\Auth\LoginRequest;
use JvgTest\Domain\ApiResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AuthController
 * @package AppBundle\Controller
 */
class AuthController extends AbsMainController
{
    public function homeAction(){
        die("JVG Test API");
    }
    /**
     * @OA\Post(
     *     path="/api/login",
     *     summary="Login endpoint",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="username",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 example={"username": "test", "password": "1111"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Login successful",
     *         @OA\JsonContent(
     *              type="string",
    *               example="{'token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjgsInVzZXJVc2VybmFtZSI6ImpvYW4iLCJ1c2VyTmFtZSI6IkpvaGFuXG4ifQ.ibEEy9yOQ40CaktQiohhN8lsXPzPZNUkDgvRvcTNzsA'}"
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Login error",
     *     )
     * )
     *
     *
     */
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request): JsonResponse
    {
        $data = $this->getRequestData($request);
        $response = new ApiResponse($data);

        try{
            $requiredFields = ['username', 'password'];
            $this->validateRequest($requiredFields, $data);

            $auth = $this->get("jvgtest.application.service.auth.login_service")->login(
                new LoginRequest($data['username'], $data['password']));


            $response->setBody($auth);
        }catch(\Exception $exception){
            $response->setHttpCode($exception->getCode());
            $response->setBody($exception->getMessage());
        }

        return $this->output($response);
    }

    /**
     * @param array $requiredFields
     * @param array|null $requestData
     * @throws AuthException
     */
    private function validateRequest(array $requiredFields, ?array $requestData): void
    {
        foreach($requiredFields as $field){
            if(empty($requestData[$field])){
                throw new AuthException("{$field} not provided",400);
            }
        }
    }
}
