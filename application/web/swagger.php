<?php
require("../vendor/autoload.php");
$openapi = \OpenApi\scan('../src/AppBundle/Controller');
if(!empty($_GET["inline"]) and $_GET["inline"] = 1){
    header('Content-Type: application/json');
    echo $openapi->toJson();
}else{
    header('Content-Type: application/x-yaml');
    echo $openapi->toYaml();
}


